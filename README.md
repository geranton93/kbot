# KBot

## Description
This is a Telegram bot designed to perform various tasks. It responds to specific commands and provides information to the users.

## Getting Started
To start using the bot, follow these simple steps:
1. Open the Telegram app.
2. Search for **@geranton93_bot** or follow this link: [Geranton93 Bot](https://t.me/geranton93_bot).
3. Start a chat with the bot by clicking the "Start" button.

## Commands
The following are the available commands for the bot:
- `/start hello`: Displays the current version of the bot.

## Usage
1. Set the `TELE_TOKEN` environment variable with your Telegram bot token.
2. Use the command `./kbot start` to start the project.

## Contributing
If you want to contribute to this project or have any suggestions, please feel free to contact the bot developer.

## License
This project is licensed under the [MIT License](https://opensource.org/licenses/MIT).

## Essays

1. [Why GitLab is Better Than Jenkins](./essays.md) - An essay highlighting seven reasons why GitLab excels over Jenkins in the CI/CD landscape.

## Gitleaks Pre-Commit Hook

This pre-commit hook uses [gitleaks](https://github.com/zricethezav/gitleaks) to scan your Git project for potential secrets before each commit. If gitleaks detects secrets, the commit will be rejected.

### Installation

1. **Move pre-commit script to hooks folder:**

   ```bash
   cp scripts/pre-commit .git/hooks/ 
   ```

2. **Make the script executable:**

   ```bash
   chmod +x .git/hooks/pre-commit
   ```

3. **Test the pre-commit hook:**

   Now, when you make a new commit, the pre-commit hook will automatically run gitleaks to check for secrets. If gitleaks finds any secrets, the commit will be rejected.

   ```bash
   git commit -m "Your commit message"
   ```

### Notes

This script creates `.gitleaks` folder in your project directory and install gitleaks there.
